from os import getpid
import pika
import logging


LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
              '-35s %(lineno) -5d: %(message)s')
LOGGER = logging.getLogger(__name__)


class BaseClient(object):
    """ Base Client class implements the base functionality of all clients.
    All the oher clients will derive from it."""

    def __init__(self,
                user_login='guest',
                password='guest',
                identifier='',
                scenario=None,
                client=None):
        """
        :param user_login: string: user login
        :param password: string: user password
        :param identifier: string: client identifier
        :param client: a BaseClient object: gives the possibility to instantiate a client using
            an already existing client object, useful when registering
        :param attributes: a dictionary of elements that will become attributes
            of the class and can be accessed via `self.`
        :return: a BaseClient instance
        """

        self.connection = None
        self.request_channel = None
        self.response_channel = None
        self.response_queue = None
        self.login = user_login
        self.password = password
        self.response_queue_name = ''
        self.identifier = identifier
        self._scenario = scenario
        if client:
            self.connection = client.conneciton
            self.request_channel = client.request_channel
            self.response_channel = client.response_channel
            self.response_queue = client.response_queue
            self.login = client.login
            self.password = client.password
            self.response_queue_name = client.response_queue_name
            self.identifier = client.identifier
            self._scenario = client.get_scenario()

        if user_login and password:
            credentials = pika.credentials.PlainCredentials(self.login, self.password)
            host = '178.17.172.60'
            virtual_host = '/ts'
            try:
                params = pika.ConnectionParameters(host=host, virtual_host=virtual_host, credentials=credentials)
                self.connection = pika.BlockingConnection(params)
            except AttributeError:
                pass
        if not identifier:
            self.identifier = '%s: %i' % (self.__class__.__name__, getpid())

    def set_scenario(self, scenario):
        """  This method will be called explicitly from `on_message_received()` method and depending on the correlation_id
        it will handle the messages accordingly.
        :param scenario: a method that allows the client handle a test scenario.
        """
        self._scenario = scenario

    def get_scenario(self):
        """ This method is used to get the client's current scenario.
        Note that this method is mostly used when initialising a new client from an existing one.
        :return: the current scenario of the client.
        """

        return self._scenario

    def consume(self):
            """ This function is intended to permanently consume messages from the temp queue.
            """
            try:
                self.response_channel.basic_consume(self._scenario, self.response_queue_name)
            except KeyboardInterrupt:
                self.response_channel.close()
                self.request_channel.close()
                self.connection.close()

    def start_consuming(self):
        """
        This method is used to continuously consume messages from queue.
        It will wait until a message is received, or the client will die.
        """
        self.response_channel.start_consuming()

    def __str__(self):

        """ This method is used to override the built-in `__str__` method in order to get a meaningful
        representation on the screen.
        :return:
        """
        return '%s:(%s)' % (self.identifier, type(self))

if __name__ == '__main__':
    client = BaseClient()
