from base_client import BaseClient
import logging
import pika
from messages import selectOffer
from utils.definitions.correlation_id_values import NEW_ORDER, SELECT_OFFER, MAKE_OFFER, PROVIDER_ARRIVED,\
    GET_PROP, RATE_PROVIDER, ASSIGN_ORDER
import json

LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
              '-35s %(lineno) -5d: %(message)s')
LOGGER = logging.getLogger(__name__)

ORDER_ID = None


def on_make_offer_handler(self, basic_deliver, properties, body):
    raw_input('\n\nDo you want to select first one? Press enter!')
    payload = json.loads(body)
    LOGGER.info('Received offer from: %s. Estimated time: %s'% (properties.reply_to, payload['pickup_time']))
    message = selectOffer(self.response_queue_name)
    message.properties.headers['reply-to-provider'] = properties.headers['reply-to-provider']
    message.properties.headers['order-number'] = properties.headers['order-number']
    message.set_payload(**payload)
    self.publish(message=message)


customer_correlation_id_map = {
    MAKE_OFFER: on_make_offer_handler
}


class CustomerClient(BaseClient):
    """ Customer client to simulate the driver android app.
    """
    temp_queue_name = "/temp-queue/q_customer-%s"
    _exchange = 'e_requests_customer'

    def __init__(self, **kwargs):
        # call the parent class constructor
        super(CustomerClient, self).__init__(**kwargs)
        self.subscribe()
        self.publish_init()

    def subscribe(self):
        """ This method is called to initialise the consumer part of the customer client.
         It is called right after the super class of DriverClient constructor is invoked.
        """
        # Create a channel for the consumer part: response_channel
        if not self.response_channel:
            self.response_channel = self.connection.channel()
            LOGGER.info('Response channel initialised!')
        # If we are here then we already have the response channel setup done.
        if not self.response_queue:
            try:
                self.response_queue_name = self.temp_queue_name % self.login
                print self.response_queue_name
                self.response_queue = self.response_channel.queue_declare(self.response_queue_name,
                                                                          auto_delete=True)
                LOGGER.info('Connected as %s:%s', self.login, self.password)
            except pika.exceptions.ProbableAccessDeniedError as e:
                LOGGER.warning('Access Denied!')
        # if we got here, then, the response queue is created, and we can consume from it.
        # checking if there are some messages already in the queue.(because why not!)
        self.response_channel.basic_consume(self.on_message_received, self.response_queue_name)

    def publish_init(self):
        """ This method is called to initialise the publisher part of the customer cilent.
        """
        self.request_channel = self.connection.channel()
        LOGGER.info('Request channel initialised.')

    def acknowledge_message(self, delivery_tag):
        """
        This method is called when a message is received.
        :param delivery_tag: The delivery tag from the Basic.Deliver frame.
        :return:
        """
        LOGGER.info('Acknowledging message %s', delivery_tag)
        self.response_channel.basic_ack(delivery_tag)

    def on_message_received(self, channel, basic_deliver, properties, body):
        """ This method will manage the flow of messages. Invoked by pika when a message is delivered from RabbitMQ.
        :param channel: Channel object.
        :param basic_deliver: carries the exchange, routing key, delivery tag and
        a redelivered flag for the message.
        :param properties: an instance of BasicProperties with the message properties
        :param body: message payload.
        :return:
        """
        LOGGER.info('Received message # %s with %s: %s',
                    basic_deliver.delivery_tag, properties, body)

        if properties.correlation_id:
            correlation_id = properties.correlation_id
            print 'correlation_id: ', correlation_id
        else:
            LOGGER.error('NO correlation_id present in headers!')
            return
        misc = raw_input('\n\nPress enter to continue!')
        try:
            customer_correlation_id_map[correlation_id](self, basic_deliver, properties, body)
        except KeyError as e:
            LOGGER.error('customer_correlation_id does not support the given correlation_id. %s ', e)
        finally:
            self.acknowledge_message(basic_deliver.delivery_tag)


    def publish(self, exchange=_exchange, message=None):
        """ This method is called to publish given messages to RabbitMQ.
        :param exchange: the exchange to publish to.
        :param message: an instance of a AMQP message wrapper.
        """

        self.request_channel.basic_publish(exchange,
                                           '',
                                           body=message.payload,
                                           properties=message.properties)
        LOGGER.info('Published message # %s, %s', message.properties, message.payload)

    def consume(self):
        """ This function is intended to permanently consume messages from the temp queue.
        """
        try:
            self.response_channel.start_consuming()
        except KeyboardInterrupt:
            self.response_channel.close()
            self.request_channel.close()
            self.connection.close()


def main():
    logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)

    from messages import getProp

    customer = CustomerClient(user_login='TestCustomer', password='willNotBuy')

    message = getProp(customer.response_queue_name)
    customer.publish(message=message)
    print "time to consume!"
    customer.consume()


if __name__ == '__main__':
    main()