from base_client import BaseClient
import logging
from messages import makeOffer, assignProvider
import pika
import json
from utils.definitions.correlation_id_values import NEW_ORDER, SELECT_OFFER,\
    MAKE_OFFER, PROVIDER_ARRIVED, ASSIGN_ORDER, CUSTOMER_BOARDED, REFUSE_ORDER,\
    PROVIDER_CLOSED

LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
              '-35s %(lineno) -5d: %(message)s')
LOGGER = logging.getLogger(__name__)

CURRENT_ORDER_ID = None


def on_new_order_handler(self, basic_deliver, properties, body):
    global CURRENT_ORDER_ID
    CURRENT_ORDER_ID = properties.headers['order-number']
    LOGGER.info('New order received! Order_id: %s', CURRENT_ORDER_ID)
    message = makeOffer(queue_name=self.response_queue_name)
    message.properties.headers['order-number'] = CURRENT_ORDER_ID
    message.properties.headers['reply-to-customer'] = properties.headers['reply-to-customer']
    message.set_payload(pickup_time=5)

    self.publish(message=message)


def on_select_offer_handler(self, basic_deliver, properties, body):
    #payload = json.loads(body)
    message = assignProvider(self.response_queue_name)
    message.properties.headers['reply-to-customer'] = self.response_queue_name
    message.properties.headers['order-number'] = properties.headers['order-number']
    #message.set_payload(**payload)
    LOGGER.info('ACKing that the driver is on the way! ')
    self.publish(message=message)


driver_correlation_id_map = {
    NEW_ORDER: on_new_order_handler,
    SELECT_OFFER: on_select_offer_handler
}


class DriverClient(BaseClient):
    """ Driver client to simulate the driver android app.
    """
    temp_queue_name = "/temp-queue/q_provider-%s"
    _exchanges_map = {'consume':'e_orders_broadcast',
                      'publish':'e_requests_provider'}

    def __init__(self, **kwargs):
        # call the parent class constructor
        super(DriverClient, self).__init__(**kwargs)
        self.subscribe()
        self.publish_init()

    def subscribe(self):
        """ This method is called to initialise the consumer part of the driver client.
         It is called right after the super class of DriverClient constructor is invoked.
        """
        # Check if there already exist a response_channel.
        # if nope, Create a channel for the consumer part: response_channel
        if not self.response_channel:
            self.response_channel = self.connection.channel()
        try:
            self.response_queue_name = self.temp_queue_name % self.login
            self.response_queue = self.response_channel.queue_declare(self.response_queue_name, auto_delete=True)
            LOGGER.info('Connected as %s:%s', self.login, self.password)
            try:
                self.response_channel.queue_bind(exchange=self._exchanges_map['consume'],
                                                queue=self.response_queue_name,
                                                routing_key='filter.text3.blue.green')
            except:
                raise AssertionError
        except AssertionError:
            LOGGER.warning('Binding failed!')
        except pika.exceptions.ProbableAccessDeniedError as e:
            LOGGER.warning('Access Denied!')
        # if we got here, then, the response queue is created, and we can consume from it.
        # checking if there already exist messages int the queue.
        self.response_channel.basic_consume(self.on_message_received, self.response_queue_name)

    def publish_init(self):
        """ This method is called to initialise the publisher part of the customer cilent.
        """
        self.request_channel = self.connection.channel()
        LOGGER.info('Request channel initialised.')

    def acknowledge_message(self, delivery_tag):
        """
        This method is called when a message is received.
        :param delivery_tag: The delivery tag from the Basic.Deliver frame.
        :return:
        """
        LOGGER.info('Acknowledging message %s', delivery_tag)
        self.response_channel.basic_ack(delivery_tag)

    def on_message_received(self, channel, basic_deliver, properties, body):
        """
        Invoked by pika when a message is delivered from RabbitMQ. T
        :param channel: Channel object.
        :param basic_deliver: carries the exchange, routing key, delivery tag and
        a redelivered flag for the message.
        :param properties: an instance of BasicProperties with the message properties
        :param body: message payload.
        :return:
        """
        LOGGER.info('Received message # %s from %s: %s',
                    basic_deliver.delivery_tag, properties, body)

        if properties.correlation_id:
            correlation_id = properties.correlation_id
            print 'correlation_id: ', correlation_id
        else:
            LOGGER.error('NO correlation_id present in headers!')
            return

        misc = raw_input("\n\nPress enter to continue!")
        try:
            driver_correlation_id_map[correlation_id](self, basic_deliver, properties, body)
        except KeyError as e:
            if 'response' in properties.headers:
                if properties.headers['response'] in ['ARRIVAL_ACK', 'SELECT_ACK', 'OFFER_ACK']:
                    LOGGER.info('Received %s ACK!', correlation_id)
                else:
                    LOGGER.error('driver_correlation_id does not support the given correlation_id. %s ', e)
            else:
                    LOGGER.info('some interesting message received.', )
        finally:
            self.acknowledge_message(basic_deliver.delivery_tag)

    def publish(self, exchange=_exchanges_map['publish'], message=None):
        """ This method is called to publish given messages to RabbitMQ.
        :param exchange: the exchange to publish to.
        :param message: an instance of a AMQP message wrapper.
        """
        self.request_channel.basic_publish(exchange,
                                           '',
                                           body=message.payload,
                                           properties=message.properties)
        LOGGER.info('Published message # %s, %s', message.properties, message.payload)

    def consume(self):
        """ This function is intended to permanently consume messages from the temp queue.
        """
        try:
            self.response_channel.start_consuming()
        except KeyboardInterrupt:
            self.response_channel.close()
            self.request_channel.close()
            self.connection.close()







def main():
    logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)
    driver = DriverClient(user_login="VaneaDoe", password='123456789')
    #driver = DriverClient(user_login="armstrong", password='11111')
    driver.consume()


if __name__ == '__main__':
    main()

