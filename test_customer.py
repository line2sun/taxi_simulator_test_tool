from clients.customer import CustomerClient
from messages import getPOI, getProp, newOrder


import logging
import os,sys,inspect


current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
print current_dir

parent_dir = os.path.dirname(current_dir)
print parent_dir
sys.path.insert(0,parent_dir)


LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
              '-35s %(lineno) -5d: %(message)s')
LOGGER = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)







customer = CustomerClient(user_login='guest', password='guest')

#message = getPOI(customer.response_queue_name)
#message = getProp(customer.response_queue_name)
message = newOrder(customer.response_queue_name)
customer.publish(message=message)
customer.consume()
