from clients.customer import CustomerClient
#from clients.driver import DriverClient
import logging
import os
os.chdir('messages')
from messages import register

LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
              '-35s %(lineno) -5d: %(message)s')
LOGGER = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)


customer = CustomerClient(user_login='TestCustomer', password='willNotBuy')
message = register(customer.response_queue_name)
customer.publish(message=message, exchange='e_register')
customer.consume()
