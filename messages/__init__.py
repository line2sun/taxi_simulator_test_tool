import yaml
from message import Message
import os

config_file = os.path.abspath('/Users/line2sun/PycharmProjects/simulator_taxi_clients/messages/etc/messages.yaml')
print 'config: ', config_file

yaml_data = open(config_file, 'r')
messages_map = yaml.load(yaml_data)
yaml_data.close()


def select_message(request_type, queue_name='q_test'):
    print "__init__ message: %s"% request_type
    return Message(message_dict=messages_map[request_type], reply_to=queue_name)

def getPOI(queue_name='q_test'):
    return select_message('getPOI', queue_name)

def newOrder(queue_name='q_test'):

    return select_message('newOrder', queue_name)

def register(queue_name='q_test'):
    return select_message('register', queue_name)

def registerAck(queue_name='q_test'):
    return select_message('registerAck', queue_name)

def makeOffer(queue_name='q_test'):
    return select_message('makeOffer', queue_name)

def selectOffer(queue_name='q_test'):
    return select_message('selectOffer', queue_name)

def chatMessage(queue_name='q_test', chat_message='banana'):
    result = select_message('chatMessage', queue_name)
    result['payload']['message'] = chat_message
    return result

def getProp(queue_name='q_test'):
    return select_message('getProp', queue_name)

def rateProvider(queue_name='q_test'):
    return select_message('rateProvider', queue_name)


def assignProvider(queue_name='q_test'):
    return select_message('assignOrder', queue_name)

def customerBoarded(queue_name='q_test'):
    return select_message('customerBoarded', queue_name)

def providerArrived(queue_name='q_test'):
    return select_message('providerAssigned', queue_name)

def orderFinished(queue_name='q_test'):
    return select_message('orderFinished', queue_name)


if __name__ == '__main__':
    print orderFinished()
    print customerBoarded()
    print providerArrived()
    print rateProvider()
    print assignProvider()
    print selectOffer()
    print getPOI()
    print getProp()
    print newOrder()
    print makeOffer()
    print chatMessage()
    print register()
    print registerAck()
    print 'DONE!'