import json
import pika


class Message(object):
    """
    This is a wrapper for the AMQP messages.
    """
    def __init__(self, message_dict=None, reply_to=None):
        if not message_dict:
            return
        #print message_dict
        self.payload = json.dumps(message_dict['payload'])

        self.properties = pika.BasicProperties(reply_to=reply_to,
                                               correlation_id=message_dict['properties']['correlation_id'],
                                               headers=message_dict['headers'])

    def set_payload(self, **kwargs):
        self.payload = json.dumps(kwargs)

    def __str__(self):
        dict_rep = dict()
        dict_rep['payload'] = self.payload
        dict_rep['properties'] = str(self.properties)
        return str(dict_rep)

    # TO DO: Create an abstract way of creating messages, from .yaml file, for example.


