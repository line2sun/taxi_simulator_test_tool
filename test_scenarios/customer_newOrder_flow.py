from messages import newOrder
from utils.definitions.correlation_id_values import *
import logging

LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
              '-35s %(lineno) -5d: %(message)s')
LOGGER = logging.getLogger(__name__)

ORDER_ID = None

def on_new_order_handler(self, channel, basic_deliver, properties, body):
    global ORDER_ID
    ORDER_ID = body
    LOGGER.info('ORDER_ID: %s', ORDER_ID)

    # ACKing RabbitMQ
    self.publish


def on_select_offer_handler(channel, basic_deliver, properties, body):
    pass


def on_chat_message_handler():
    pass


def on_get_active_order_handler():
    pass


def on_get_poi_handler():
    pass


def on_set_prop_handler():
    pass


def on_get_prop_handler():
    pass


def on_rate_provider_handler():
    pass


def on_register_handler():
    pass


def on_register_ack_handler():
    pass


def on_customer_closed_handler():
    pass


correlation_id_handlers_map = {
    NEW_ORDER: on_new_order_handler,
    SELECT_OFFER: on_select_offer_handler,
    CHAT_MESSAGE: on_chat_message_handler,
    GET_ACTIVE_ORDER: on_get_active_order_handler,
    GET_POI: on_get_poi_handler,
    GET_PROP: on_get_prop_handler,
    SET_PROP: on_set_prop_handler,
    RATE_PROVIDER: on_rate_provider_handler,
    REGISTER: on_register_handler,
    REGISTER_ACK: on_register_ack_handler,
    CUSTOMER_CLOSED: on_customer_closed_handler
}



def scenario(self, channel, basic_deliver, properties, body):
    """  This method is called at any time a message is received into client's `/temp-queue/...'
    :param channel: Channel object.
    :param basic_deliver: carries the exchange, routing key, delivery tag and
    a redelivered flag for the message.
    :param properties: an instance of BasicProperties with the message properties
    :param body: message payload.
    """
    try:
        correlation_id = properties['correlation_id']
        LOGGER.info('Correlation_id: %s' % correlation_id)
        if correlation_id in [NEW_ORDER, SELECT_OFFER, PROVIDER_ARRIVED, ASSIGN_ORDER]:
            result = correlation_id_handlers_map[correlation_id](self, channel, basic_deliver, properties, body)
        else:
            LOGGER.debug('No such correlation_id handler!')
    except KeyError as e:
        LOGGER.debug('No such correlation_id headers: %s' % e)
    finally:
        LOGGER.info('Received message # %s with %s: %s',
                    basic_deliver.delivery_tag, properties, body)
        self.acknowledge_message(basic_deliver.delivery_tag)
